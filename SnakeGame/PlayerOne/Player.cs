﻿using System;
using System.Collections.Generic;
using Crestron.SimplSharpPro.CrestronThread;
using Crestron.SimplSharp;
using SnakeGame.Managers;
using SnakeGame.Grid;

namespace SnakeGame.PlayerOne
{
    class Player
    {
        /* Event that will be triggered when the play head overlaps the consumable. */
        public delegate void ConsumableEatenEventHandler(object sender, EventArgs args);
        public static event ConsumableEatenEventHandler OnConsumableEaten;

        private List<Cell> playerCells = new List<Cell>();
        private Cell head;                                  // The head of the snake.
        private Cell tail;                                  // The tail of the snake.
        private CTimer timer;                               // Timer that will move the snake along the grid.
        private int newHeadIndex = 1;                       // The next cell in line to become the new head of the snake.

        public List<Cell> PlayerCells 
        { 
            get { return playerCells; }    
        }
        public static int XModifier { private get; set; }   // Will modify the the direction the snake is moving on the x axis.
        public static int YModifier { private get; set; }   // Will modify the direction the snake is moving on the y axis.

        public Player()
        {
            // Default snake size and move direction at the start of the game.
            playerCells.Add(new Cell("snakeHead", 7, 9));
            playerCells.Add(new Cell("snakeTail",7, 8));
            XModifier = 1;
            YModifier = 0;
            GameManager.OnGameStatusChange += Instance_OnGameStatusChange;
        }
        private void MoveSnake(object input)
        {
            // Figures what which index in the array is the head of the snake on the GUI.
            int cellInfoToGrab = newHeadIndex == playerCells.Count - 1 ? 0 : newHeadIndex + 1;

            // Finds the new tail and new head of the snake.
            tail = new Cell("snakeTail", playerCells[newHeadIndex].Y, playerCells[newHeadIndex].X);
            head = playerCells[newHeadIndex] = new Cell("snakeHead", playerCells[cellInfoToGrab].Y + YModifier, playerCells[cellInfoToGrab].X + XModifier);

            // Ready to accept a new direction from the inputmanager.
            InputManager.Instance.NewDirection = false;

            // Collision detection. Checks to see if the snake collided with itself or if it has collided with the grid borders.
            if ((head.Y < 0 || head.Y > 17 || head.X < 0 || head.X > 19) || GameManager.Instance.CellGrid[head.Y, head.X]?.Name.Contains("snake") == true)
            {
                GameManager.Instance.GameOver();
            }
            else
            {
                Cell cell = GameManager.Instance.CellGrid[head.Y, head.X];

                // Deactivates the tail of the snake and activates the new head of the snake.
                GameManager.Instance.UpdateCellGrid(head);
                GameManager.Instance.UpdateCellGrid(tail);
                
                // Gets the index of the next cell that will become the new head.
                newHeadIndex = (newHeadIndex - 1) < 0 ? playerCells.Count - 1 : --newHeadIndex;

                // Checks to see if the snake collided with a a consumable.
                if (cell?.Name == "consumable")
                {
                    OnConsumableEaten?.Invoke(this, new EventArgs());
                    ScoreManager.Instance.AddPoint();
                    timer.Dispose();
                    Thread t = new Thread(AddPlayerCell, null, Thread.eThreadStartOptions.Running);
                }
            }
        }
        private object AddPlayerCell(object input)
        {
            // This function will add a new cell at the index of the current head. This cell will become the new snake head on the next update.
            // Values 18,0 were used for the cordinates because we don't want any of the snake cells to deactivate on the next update. 
            // Once the new cell has been added to the snake body, the move timer will be started again.
            int index = playerCells.IndexOf(head);

            if (index == 0) {
                playerCells.Add(new Cell("placeHolder", 18, 0));
                newHeadIndex = playerCells.Count - 1;
            }
            else {
                playerCells.Insert(index, new Cell("placeHolder", 18, 0));
                newHeadIndex = index;
            }
            timer = new CTimer(MoveSnake, null, 100, 100);
            return null;
        }
        private void Instance_OnGameStatusChange(object sender, GameManagerEventArgs args)
        {
            if (args.GameStarted)
            {
                XModifier = 1;
                YModifier = 0;
                timer = new CTimer(MoveSnake, null, 200, 100);
            }
            else
            {
                timer.Dispose();
                GameManager.OnGameStatusChange -= Instance_OnGameStatusChange;
            }
        }
    }
}
