﻿using System;
using Crestron.SimplSharpPro.CrestronThread;
using SnakeGame.Managers;
using SnakeGame.PlayerOne;
using SnakeGame.Grid;

namespace SnakeGame.Consumables
{
    class Consumable
    {
        private Random rand = new Random();     // Will help us generate a random location for the consumable.
        bool clearToSpawn;                      // Is the consumable clear to spawn.
        public Cell Cell { get; private set; }  // Cell info for the consumable.

        public Consumable()
        {
            GameManager.OnGameStatusChange += GameManager_OnGameStatusChange;
            Player.OnConsumableEaten += Player_OnFoodEaten;
            Cell = new Cell("consumable", 5, 15);
        }
        private object SpawnConsumable(object input)
        {
            while (!clearToSpawn)
            {
                Cell = new Cell("consumable", rand.Next(0, 17), rand.Next(0, 19));

                /* Checks to see if the location that the consumable will attempt to spawn at is empty. */
                if(GameManager.Instance.CellGrid[Cell.Y, Cell.X] == null)    
                    clearToSpawn = true;
            }

            clearToSpawn = false;
            GameManager.Instance.UpdateCellGrid(Cell);
            return null;
        }
        private void GameManager_OnGameStatusChange(object sender, GameManagerEventArgs args)
        {
            /* Unsubscribe from events when ever there is a game over.*/
            if (!args.GameStarted)
                Player.OnConsumableEaten -= Player_OnFoodEaten;
        }
        private void Player_OnFoodEaten(object sender, EventArgs args)
        {
            Thread t = new Thread(SpawnConsumable, null, Thread.eThreadStartOptions.Running);
        }
    }
}
