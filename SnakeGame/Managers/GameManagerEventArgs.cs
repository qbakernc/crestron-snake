﻿using System;

namespace SnakeGame.Managers
{
    public class GameManagerEventArgs : EventArgs
    {
        public bool GameStarted { get; private set; }
        public GameManagerEventArgs(bool gameStarted)
        {
            GameStarted = gameStarted;
        } 
    }
}
