﻿using System.Collections.Generic;
using Crestron.SimplSharp;                          
using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharpPro.CrestronThread;


namespace SnakeGame.Managers
{
    class ScoreManager
    {
        // Event that will be triggered once once it has been determined if a new score has been found when the snake dies.
        public delegate void NewHighScoreEventHandler(object sender, ScoreManagerEventArgs args);
        public static event NewHighScoreEventHandler OnNewHighScore;

        private XpanelForHtml5 uI;
        private CTimer timer;
        private List<uint> highScores = new List<uint> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };                                                        // Will hold the top 10 high scores.
        private List<string> highScoresInitials = new List<string> {"___", "___", "___", "___", "___", "___", "___", "___", "___", "___" };     // Will hold the initials of the top 10 high scores.
        private bool newHighScoreAchieved;                                                                                                      // Has a new high score been achieved. 
        private int newHighScoreIndex;                                                                                                          // The players placement on the high score list.
        private string newHighScoreInitials = "";                                                                                               // Will hold the players initials.
        private int score = 0;                                                                                                                  // The score of the active game session.

        private static ScoreManager instance;   // Singleton Pattern. Only one instace of this class can be used.
        public static ScoreManager Instance
        {
            get {
                if (instance == null) {
                    instance = new ScoreManager();
                }
                return instance;
            }
        }
        public XpanelForHtml5 UI
        {
            set {
                uI = value;
            }
        }
        public List<uint> HighScores
        {
            get { return highScores; }
        }
        public List<string> HighScoresInitials
        {
            get { return highScoresInitials; }
        }
        public ScoreManager()
        {
            GameManager.OnGameStatusChange += GameManager_OnGameStatusChange;
            InputManager.OnInitialsComplete += InputManager_OnInitialsComplete;
        }

        private void InputManager_OnInitialsComplete(object sender, InputManagerEventArgs args)
        {
            timer.Dispose();
            newHighScoreInitials = args.Initials;
            uI.BooleanInput[1013].BoolValue = false;
            uI.StringInput[1000].StringValue = "0";
            Thread t = new Thread(InsertNewHighScore, null, Thread.eThreadStartOptions.Running);
            
        }

        private void GameManager_OnGameStatusChange(object sender, GameManagerEventArgs args)
        {
            if (!args.GameStarted) {
                CompareScoreToList();
            }
        }

        public void AddPoint()
        {
            score++;
            uI.StringInput[1000].StringValue = score.ToString();
        }
        private void FlashNewHighScoreLabel(object inpur)
        {
            uI.BooleanInput[1013].BoolValue = !uI.BooleanInput[1013].BoolValue;
        }
        private void CompareScoreToList()
        {
            for (int i = 0; i < highScores.Count; i++) {
                if (score > highScores[i]) {
                    newHighScoreAchieved = true;
                    newHighScoreIndex = i;      
                    break;
                }
            }
            if (!newHighScoreAchieved) {
                score = 0;

                // The player did not achieve a high score. The game will be reset.
                OnNewHighScore?.Invoke(this, new ScoreManagerEventArgs(false, score));
            }
            else {
                // The player did achieve a high score. Player will be prompted to enter in his/her initials.
                OnNewHighScore?.Invoke(this, new ScoreManagerEventArgs(true, score));
                timer = new CTimer(FlashNewHighScoreLabel, null, 0, 500);
            }
        }

        public object InsertNewHighScore(object input)
        {
            highScores.Insert(newHighScoreIndex, (uint)score);
            highScores.RemoveAt(highScores.Count - 1);
            highScoresInitials.Insert(newHighScoreIndex, newHighScoreInitials.ToUpper());
            highScoresInitials.RemoveAt(highScoresInitials.Count - 1);
            newHighScoreInitials = "";
            newHighScoreAchieved = false;
            score = 0;

            // The player did not achieve a high score. The game will be reset.
            OnNewHighScore?.Invoke(this, new ScoreManagerEventArgs(false, score));
            return null;
        }
    }
}
