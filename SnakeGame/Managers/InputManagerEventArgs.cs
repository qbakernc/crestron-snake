﻿using System;

namespace SnakeGame.Managers
{
    class InputManagerEventArgs : EventArgs
    {
        public string Initials { get; private set; }
        public InputManagerEventArgs(string initials)
        {
            Initials = initials;
        }
    }
}
