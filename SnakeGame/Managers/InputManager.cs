﻿using System;
using System.Text;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronSockets;
using Crestron.SimplSharpPro.UI;
using SnakeGame.PlayerOne;

namespace SnakeGame.Managers
{
    class InputManager
    {
        // event that will be triggered once the player is done entering their initials at the high score screen.
        public delegate void InitialsInputEventHandler(object sender, InputManagerEventArgs args);
        public static event InitialsInputEventHandler OnInitialsComplete;

        TCPServer tcpServer = new TCPServer("0.0.0.0", 10000, 10000, EthernetAdapterType.EthernetLANAdapter, 1);
        XpanelForHtml5 uI;
        CTimer timer;
        CTimer inputDelay;
        private const string PossibleMovementKeys = "wdsa";                 // All of the possible player movement inputs.
        private const string HorizontalMovement = "a d";                    // Buttons for left and right.
        private const string VerticalMovement = "w s";                      // Buttons for up and down.
        private const string InitialsInput = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";  // Possible inputs when entering in your initials for the high score.
        private string rcvInput;                                            // String received from the tcp client.
        private string previousMovementKey = "d";                           // Will hold the previous movment input direction.
        private bool keyPressed;                                            // Has a key been pressed.
        private bool gameStarted;                                           // Has the game started.
        private bool gameRefreshing;                                        // Has the game fully reset.
        private bool NewHighScore;                                          // Is the player entering in their name for a high score.
        private string initials = "A__";                                    // Will hold the players initials 
        private int counter;                                                // Will be used to count the number of initials that have been put in
        private int initialsIndex;                                          // index of the InitiealsInput constant. Will change as we scroll through the letters.
        private int score;                                                  // The players high score.

        private static InputManager instance;                               // Singleton Pattern. Only one instace of this class can be used.
        public static InputManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InputManager();
                }
                return instance;
            }
        }
        public XpanelForHtml5 UI
        {
            set {
                uI = value;
            }
        }
        public bool NewDirection { get; set; }
        private InputManager()
        {
            GameManager.OnGameStatusChange += GameManager_OnGameStatusChange;
            ScoreManager.OnNewHighScore += ScoreManager_OnNewHighScoreFound;
            SocketErrorCodes error = tcpServer.WaitForConnectionAsync(ServerConnectedCallback);
        }

        private void ScoreManager_OnNewHighScoreFound(object sender, ScoreManagerEventArgs args)
        {
            NewHighScore = args.NewHighScore;
            if (NewHighScore) {
                score = args.Score;
                uI.StringInput[1011].StringValue = String.Format("{0}: {1}", initials, score);
                ListenForDataInput(null);
            }
        }

        private void GameManager_OnGameStatusChange(object sender, GameManagerEventArgs args)
        {
            gameStarted = args.GameStarted;

            if (!gameStarted)  {
                previousMovementKey = "d";
                ListenForDataInput(null);
            }
        }
        private void ServerConnectedCallback(TCPServer myTCPServer, uint clientIndex)
        {
            if (clientIndex != 0) {
                /*Starts listeing for data from the client index that just connected to the server. */
                myTCPServer.ReceiveDataAsync(clientIndex, ServerDataReceivedCallback);

                /* The server will stop listening for client connections if the max number of clients connected has been reached.*/
                if (myTCPServer.MaxNumberOfClientSupported == myTCPServer.NumberOfClientsConnected) {
                    myTCPServer.Stop();
                    CrestronConsole.PrintLine("Client limit reached. Server will stop listening: " + myTCPServer.State);
                }
            }
            else 
            {
                /* A clientIndex of 0 could mean that the server is no longer listening, or that the TLS handshake failed when a client tried to connect.
                * In the case of a TLS handshake failure, wait for another connection so that other clients can still connect. */
                if ((myTCPServer.State & ServerState.SERVER_NOT_LISTENING) > 0) {
                    CrestronConsole.PrintLine("Server is no longer listening.");
                }
                else {
                    /* This connection failed, but keep waiting for another. */
                    myTCPServer.WaitForConnectionAsync(ServerConnectedCallback);
                }
            }

        }

        private void ServerDataReceivedCallback(TCPServer myTCPServer, uint clientIndex, int totalBytesRcv)
        {
            /* Has an input button been pressed. */
            keyPressed = true;

            if (totalBytesRcv <= 0)
            {
                // 0 or negative byte count indicates the connection has been closed. Disconnect the connected client from the server. 
                myTCPServer.Disconnect(clientIndex);

                // If server is not listening because of max number of clients, It will start listening for clients again because a client disconnected.
                if ((myTCPServer.State & ServerState.SERVER_NOT_LISTENING) > 0)
                    myTCPServer.WaitForConnectionAsync(ServerConnectedCallback);
                CrestronConsole.PrintLine("Error: server's connection with client " + clientIndex + " has been closed.");
            }
            else if (!gameRefreshing)
            {
                // Grabs the first character in the server data buffer and stores it in the rcvInput string variable. 
                rcvInput = Encoding.UTF8.GetString(myTCPServer.GetIncomingDataBufferForSpecificClient(clientIndex), 0, totalBytesRcv).Substring(0,1);

                // Will use an algorithm to determine if the direction button that was pressed is opposite of the direction that the snake going. If it is opposite, the input will do nothing.
                // Ex: If the snake is going west and the button pressed for the snake to move east is pressed, the snake will not go that way because it would collide with itself. 
                if (gameStarted && !NewHighScore && PossibleMovementKeys.Contains(rcvInput) && Math.Abs(PossibleMovementKeys.IndexOf(rcvInput) - PossibleMovementKeys.IndexOf(previousMovementKey)) != 2)
                {
                    // This algorithm will determine the snake will move on the (X,Y) grid.
                    Player.XModifier = Math.Abs(HorizontalMovement.IndexOf(rcvInput)) - 1;
                    Player.YModifier = Math.Abs(VerticalMovement.IndexOf(rcvInput)) - 1;

                    // Store the input that was just pressed so that we can compare it to the next pressed input.
                    previousMovementKey = rcvInput;
                }
                else if (rcvInput == "q" && !gameStarted && !NewHighScore)
                {
                    GameManager.Instance.StartGame(null);
                }
                else if (NewHighScore)
                {
                    switch (rcvInput)
                    {
                        case " ":
                            counter++;
                            initialsIndex = 0;
                            break;
                        case "w":
                            if (initialsIndex < 25)
                                initialsIndex++;
                            break;
                        case "s":
                            if (initialsIndex > 0)
                                initialsIndex--;
                            break;
                    }

                    if (counter == 3)
                    {
                        // All inputs will be blocked for 5 seconds giving the game time to refresh.
                        timer = new CTimer(InputTimeOut, null, 5000, 0);

                        // Reset all variables and labels once three initials have been entered.
                        gameRefreshing = true;
                        OnInitialsComplete?.Invoke(this, new InputManagerEventArgs(initials));
                        initials = "A__";
                        counter = 0;
                        uI.StringInput[1011].StringValue = "";
                        NewHighScore = false;
                    } 
                    else
                    {
                        // Shows the letters.
                        initials = initials.Remove(counter, 1);
                        initials = initials.Insert(counter, InitialsInput.Substring(initialsIndex, 1));

                        // Updates the label with the players initials.
                        uI.StringInput[1011].StringValue = String.Format("{0}: {1}", initials, score);
                    }
                }

                // 50ms delay before another direction input can be registered. 
                inputDelay = new CTimer(ListenForDataInput, null, 50, 0);
            }
            else
            {
                ListenForDataInput(null);
            }
        }
        private void InputTimeOut(object input)
        {
            gameRefreshing = false;
            tcpServer.SendData(UTF8Encoding.UTF8.GetBytes("Game Done Loading. Press q to start\n\r"), UTF8Encoding.UTF8.GetBytes("Game Done Loading. Press q to start\n\r").Length);
        }
        private void ListenForDataInput(object input)
        {
            if (keyPressed)
            {
                // Begin waiting for another message from that same client. 
                tcpServer.ReceiveDataAsync(1, ServerDataReceivedCallback);
                keyPressed = false;
            }
        }
      
    }
}
