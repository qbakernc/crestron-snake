﻿using System;
using Crestron.SimplSharp;                          	// For Basic SIMPL# Classes
using Crestron.SimplSharpPro;
using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharpPro.CrestronThread;
using SnakeGame.Consumables;
using SnakeGame.PlayerOne;
using SnakeGame.Grid;

namespace SnakeGame.Managers
{
    class GameManager
    {
        /* Event that will be triggered when the game starts and when there is a game over. */
        public delegate void GameMangerEventHandler(object sender, GameManagerEventArgs args);
        public static event GameMangerEventHandler OnGameStatusChange;

        /* Styles to make the consumable, the player, and an empty tile on the grid. */
        private const string GreenStyle = "background-color:rgb(155, 186, 90); border: 0px rgb(155, 186, 90) solid";
        private const string SnakeStyle = "background-color:rgb(39, 47, 23); border: 0px rgb(39, 47, 23) solid";
        private const string ConsumableStyle = "background-color:rgb(39, 47, 23); border: 10px rgb(155, 186, 90) solid";

        private XpanelForHtml5 uI;                      // User Interface
        private Player player = new Player();           // The Player
        private Consumable food = new Consumable();     // The Consumable
        private Cell[,] cellGrid = new Cell[19,20];     // 19 x 20 grid that houses either the players body locations or the consumable location. 
        private uint[,] grid = new uint[19, 20];        // 19 x 20 grid that houses join numbers 1 though 378. The last row isnt used. 
        private CTimer timer;                           // Timer that will flash our game over label.
        private bool gameConfigured;                    // Has the game configured itself after bootup.
        private bool gameStarted;                       // Has a game session started.

        private static GameManager instance;            // Singleton Pattern. Only one instace of this class can be used.
        public static GameManager Instance {
            get {
                if(instance == null) {
                    instance = new GameManager();
                }
                return instance;
            }
        }
        public XpanelForHtml5 UI {
            set
            {
                uI = value;
                uI.OnlineStatusChange += UI_OnlineStatusChange;
            }
        }
        public Cell[,] CellGrid { 
            get { 
                return cellGrid; 
            } 
        }
        public GameManager()
        {
            gameStarted = false;
            ScoreManager.OnNewHighScore += ScoreManager_OnNewHighScoreFound;
        }
        public void StartGame(object input)
        {
            timer.Dispose();
            uI.BooleanInput[1015].BoolValue = false;
            OnGameStatusChange?.Invoke(this, new GameManagerEventArgs(true));
            gameStarted = true;
        }
        public void GameOver()
        {
            gameStarted = false;
            timer = new CTimer(FlashGameOverLabel, null, 0, 1500);
            OnGameStatusChange?.Invoke(this, new GameManagerEventArgs(false));
        }
        private void FlashGameOverLabel(object input)
        {
            uI.BooleanInput[1015].BoolValue = !uI.BooleanInput[1015].BoolValue;
        }
        public void UpdateCellGrid(Cell cell)
        {
            // Will draw either a new snake head, an empty space to replace the tail of the snake, or a consumable.
            switch (cell.Name)
            {
                case "snakeHead":
                    uI.StringInput[grid[cell.Y, cell.X]].StringValue = SnakeStyle;
                    cellGrid[cell.Y, cell.X] = cell;
                    break;
                case "snakeTail":
                    uI.StringInput[grid[cell.Y, cell.X]].StringValue = GreenStyle;
                    cellGrid[cell.Y, cell.X] = null;
                    break;
                case "consumable":
                    uI.StringInput[grid[cell.Y, cell.X]].StringValue = ConsumableStyle;
                    cellGrid[cell.Y, cell.X] = cell;
                    break;
            }
        }
        private void ScoreManager_OnNewHighScoreFound(object sender, ScoreManagerEventArgs args)
        {
            if (!args.NewHighScore)
            {
                player = new Player();
                food = new Consumable();
                Thread t = new Thread(RefreshGame, null, Thread.eThreadStartOptions.Running);
            }
        }
        private object RefreshGame(object input)
        {
            // Will put the join numbers 1 through 378 in the guid array. This scetion is only performed once when the program boots.
            if (!gameConfigured)
            {
                uint join = 1;
                for (int y = 0; y < 19; y++)
                {
                    for (int x = 0; x < 20; x++)
                    {
                        grid[y, x] = join;
                        join++;
                    }
                }
                uI.BooleanInput[1013].BoolValue = false;                // Makes the flashing new high score label disappear.
                uI.StringInput[1011].StringValue = "";                  // Sets the new High score initials label to 0.
                uI.StringInput[1000].StringValue = "0";                 // Sets the score label to 0.
                timer = new CTimer(FlashGameOverLabel, null, 0, 1500);  // Flashes the game over label.
                gameConfigured = true;
            }

            // Will set every entry in the cellGrid array to null.Will dray the player in its default location.
            for (int y = 0; y < 19; y++)
            {
                for (int x = 0; x < 20; x++)
                {
                    uI.StringInput[grid[y,x]].StringValue = GreenStyle;
                    cellGrid[y, x] = null;
                }
            }

            // Displays the new set of high scores on the screen.
            for (uint i = 0; i < ScoreManager.Instance.HighScores.Count; i++)
            {
                uI.StringInput[i + 1001].StringValue = String.Format("{0}: {1}", ScoreManager.Instance.HighScoresInitials[(int)i], ScoreManager.Instance.HighScores[(int)i]);
            }

            // Draws the snake body and the consumable on the GUI at startup.
            uI.StringInput[grid[7, 8]].StringValue = SnakeStyle;
            uI.StringInput[grid[7, 9]].StringValue = SnakeStyle;
            UpdateCellGrid(food.Cell);
            return null;
        }

        private void UI_OnlineStatusChange(GenericBase currentDevice, OnlineOfflineEventArgs args)
        {
            if (args.DeviceOnLine) {
                Thread t = new Thread(RefreshGame, null, Thread.eThreadStartOptions.Running);
                CrestronConsole.PrintLine("Xpanel Online: " + args.DeviceOnLine);
            }
        }
    }
}
