﻿using System;

namespace SnakeGame.Managers
{
    class ScoreManagerEventArgs : EventArgs
    {
        public bool NewHighScore { get; private set; }
        public int Score { get; private set; }
        public ScoreManagerEventArgs(bool newHighScore, int score)
        {
            NewHighScore = newHighScore;
            Score = score;
        }
    }
}
