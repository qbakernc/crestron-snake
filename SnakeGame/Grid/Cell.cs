﻿namespace SnakeGame.Grid
{
    public class Cell
    {
        private string name;        // The name of the cell.
        private int y;              // y coordinate of the cell.
        private int x;              // x coordinate of the cell.

        public string Name
        {
            get { return name; }
        }
        public int Y {
            get { return y; }
        }
        public int X {
            get { return x; }
        }
        public Cell(string name, int y, int x)
        {
            this.name = name;
            this.y = y;
            this.x = x;
        }
    }
}
